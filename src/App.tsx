import { defineComponent } from 'vue'
import TestCompo from './components/TestCompo'

export default defineComponent({
  setup() {
    return () => {
      return <TestCompo />
    }
  },
})
